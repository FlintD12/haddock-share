import os, random, struct, sys
from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
from Crypto.PublicKey import RSA
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
from Crypto.Cipher import PKCS1_OAEP
from cryptography.fernet import Fernet
import rsa
import shutil
import base64
import subprocess
import threading
import time


fileTest = "batmanVtortues.mp4"


########################################################


########################################################

start = time.perf_counter()

key = get_random_bytes(16)
os.system(f".\\aescrypt.exe -e -p {key} .\\{fileTest}")

end = time.perf_counter()
print (f"Encryption time : {end - start:0.4f} secondes")




#######################################################


start = time.perf_counter()
f1 = open(fileTest,"rb")
blob1 = f1.read()
f1.close()
key = get_random_bytes(16)
cipher_aes = AES.new(key, AES.MODE_EAX)
cipher,tag = cipher_aes.encrypt_and_digest(blob1)
fd = open("cryptodome.enc", "wb")               
[ fd.write(x) for x in (key, cipher_aes.nonce, tag,cipher)]
fd.close()
end = time.perf_counter()
print (f"Encryption time : {end - start:0.4f} secondes")

