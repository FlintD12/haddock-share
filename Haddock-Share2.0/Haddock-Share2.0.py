#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, random, struct, sys
from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
import rsa
import shutil
import zlib
import base64





############################################################################




############################################################################

# https://medium.com/@ismailakkila/black-hat-python-encrypt-and-decrypt-with-rsa-cryptography-bd6df84d65bc

#Our Encryption Function
def encrypt_blob(blob, public_key):
    #Import the Public Key and use for encryption using PKCS1_OAEP
    rsa_key = RSA.importKey(public_key)
    rsa_key = PKCS1_OAEP.new(rsa_key)

    #compress the data first
    blob = zlib.compress(blob)

    #In determining the chunk size, determine the private key length used in bytes
    #and subtract 42 bytes (when using PKCS1_OAEP). The data will be in encrypted
    #in chunks
    chunk_size = 470
    offset = 0
    end_loop = False
    encrypted =  ""

    while not end_loop:
        #The chunk
        chunk = blob[offset:offset + chunk_size]

        #If the data chunk is less then the chunk size, then we need to add
        #padding with " ". This indicates the we reached the end of the file
        #so we end loop here
        if len(chunk) % chunk_size != 0:
            end_loop = True
            chunk += " " * (chunk_size - len(chunk))

        #Append the encrypted chunk to the overall encrypted file
        encrypted += rsa_key.encrypt(chunk).decode('utf8')

        #Increase the offset by chunk size
        offset += chunk_size

    #Base 64 encode the encrypted file
    return base64.b64encode(encrypted)






#Our Decryption Function
def decrypt_blob(encrypted_blob, private_key):

    #Import the Private Key and use for decryption using PKCS1_OAEP
    rsakey = RSA.importKey(private_key)
    rsakey = PKCS1_OAEP.new(rsakey)

    #Base 64 decode the data
    encrypted_blob = base64.b64decode(encrypted_blob)

    #In determining the chunk size, determine the private key length used in bytes.
    #The data will be in decrypted in chunks
    chunk_size = 512
    offset = 0
    decrypted = ""

    #keep loop going as long as we have chunks to decrypt
    while offset < len(encrypted_blob):
        #The chunk
        chunk = encrypted_blob[offset: offset + chunk_size]

        #Append the decrypted chunk to the overall decrypted file
        decrypted += rsakey.decrypt(chunk)

        #Increase the offset by chunk size
        offset += chunk_size

    #return the decompressed decrypted data
    return zlib.decompress(decrypted)


	

def encrypt_file(in_filename, out_filename=None, chunksize=64*1024):

    ext = os.path.splitext(in_filename)[1]

    if not out_filename:
        out_filename = 's:\\copy' + ext + '.enc'
    else :
        out_filename = 's:\\' + out_filename + ext + '.enc'

    fd = open("s:\\public_key.pem", "rb")
    public_key = fd.read()
    fd.close()

    fd = open(in_filename, "rb")
    unencrypted_blob = fd.read()
    fd.close()

    encrypted_blob = encrypt_blob(unencrypted_blob, public_key)

    fd = open(out_filename, "wb")
    fd.write(encrypted_blob)
    fd.close()




def decrypt_file(in_filename, out_filename=None, chunksize=24*1024):


    if not out_filename:
        out_filename = filedialog.askdirectory()+"//adhoc"+in_filename[-8:-4]

    else :
        out_filename = filedialog.askdirectory()+ '\\' + out_filename+in_filename[-8:-4]
        
    fd = open("private_key.pem", "rb")
    private_key = fd.read()
    fd.close()

    fd = open(in_filename, "rb")
    encrypted_blob = fd.read()
    fd.close()

    fd = open(out_filename, "wb")
    fd.write(decrypt_blob(encrypted_blob, private_key))
    fd.close()





############################################################################




############################################################################


class connectApp(Toplevel):

    def __init__(self,master):
        Toplevel.__init__(self,master)
        self.title("Connexion réseau - Haddock Share")
        self.geometry("200x200")
        self.iconbitmap('haddockIco.ico')
        self.grab_set()
        self.packCo()
        

    

    def connexion (self):

        reseauname = self.e1.get()
        mdp = self.passWord.get()

        contenuFic = '<?xml version="1.0"?><WLANProfile xmlns="http://www.microsoft.com/networking/WLAN/profile/v1"><name>'+reseauname+'</name><SSIDConfig><SSID><name>'+reseauname+'</name></SSID></SSIDConfig><connectionType>ESS</connectionType><connectionMode>auto</connectionMode><MSM><security><authEncryption><authentication>WPA2PSK</authentication><encryption>AES</encryption><useOneX>false</useOneX></authEncryption><sharedKey><keyType>passPhrase</keyType><protected>false</protected><keyMaterial>'+mdp+'</keyMaterial></sharedKey></security></MSM><MacRandomization xmlns="http://www.microsoft.com/networking/WLAN/profile/v3"><enableRandomization>false</enableRandomization></MacRandomization></WLANProfile>'
        ficXML = open("profile.xml", "w")
        ficXML.write(contenuFic)
        ficXML.close()
        os.system('netsh wlan add profile filename="profile.xml"')
        b = os.popen('netsh wlan connect name="'+reseauname+'"').read()
        new_key = RSA.generate(4096, e=65537)
        
        #The private key in PEM format
        private_key = new_key.exportKey("PEM")

        #The public key in PEM Format
        public_key = new_key.publickey().exportKey("PEM")

        os.system('mkdir C:\\Users\\Public\\Haddock')
        os.system('net share C:\\Users\\Public\\Haddock')

        fd = open("private_key.pem", "wb")
        fd.write(private_key)
        fd.close()

        fd = open("C:\\Users\\Public\\Haddock\\public_key.pem", "wb")
        fd.write(public_key)
        fd.close()
        self.destroy()


    def annulation (self):

        deco()
        self.destroy()
        

    def packCo(self):

        
        self.nomReseauCo = Label(self,text='Nom du réseau : ').pack()
        self.e1 = Entry(self)
        self.e1.pack()

        self.mdpReseauCo = Label(self,text='Mdp réseau : ').pack()

        self.passWord = Entry(self, show="*")
        self.passWord.pack()
        Label(self,text='').pack()
        self.buttonConnect = Button(self, text='Connecter', command=self.connexion).pack()
        Label(self,text='').pack()
        self.buttonCancel = Button(self, text='Annuler', command=self.annulation).pack()
        


############################################################################




############################################################################




class createApp(Toplevel):

    def __init__(self,master):
        Toplevel.__init__(self,master)
        self.title("Création réseau - Haddock Share")
        self.geometry("200x200")
        self.iconbitmap('haddockIco.ico')
        self.grab_set()
        self.packCreate()


    def annulation (self):

        deco()
        self.destroy()
        


    def creation (self):

        nomConnexion=self.e3.get()
        mdp=self.e4.get()
        os.system('netsh wlan set hostednetwork mode=allow ssid='+nomConnexion+' key='+mdp)
        os.system('netsh wlan start hostednetwork')
        new_key = RSA.generate(4096, e=65537)

        #The private key in PEM format
        private_key = new_key.exportKey("PEM")

        #The public key in PEM Format
        public_key = new_key.publickey().exportKey("PEM")

        os.system('mkdir C:\\Users\\Public\\Haddock')
        os.system('net share C:\\Users\\Public\\Haddock')

        fd = open("private_key.pem", "wb")
        fd.write(private_key)
        fd.close()

        fd = open("C:\\Users\\Public\\Haddock\\public_key.pem", "wb")
        fd.write(public_key)
        fd.close()

        self.destroy()

    def packCreate(self):

        self.nomReseauCreate = Label(self,text='Nom du réseau : ')
        self.nomReseauCreate.pack()
        self.e3 = Entry(self)
        self.e3.pack()
        self.mdpReseauCreate = Label(self,text='Mdp réseau : ').pack()
        self.e4 = Entry(self)
        self.e4.pack()
        Label(self,text='').pack()
        self.buttonCreate = Button(self, text='Create', command=self.creation).pack()
        Label(self,text='').pack()
        self.buttonCancel = Button(self, text='Annuler', command=self.annulation).pack()


############################################################################




############################################################################




class downloadApp(Toplevel):

    def __init__(self,master):
        Toplevel.__init__(self,master)
        self.title("Téléchargement fichier - Haddock Share")
        self.geometry("200x220")
        self.iconbitmap('haddockIco.ico')
        self.grab_set()
        self.packDownloadFile()

    def annulation (self):

        self.destroy()

    

    def downloadFile (self):

        
        filename = filedialog.askopenfilename(initialdir = "C:\\Users\Public\\Haddock\\", title="Ouvrir votre document",filetypes=[('all files','.*')])
        newName = self.nomFichier.get()
        decrypt_file(filename,newName)
        self.destroy()




    def packDownloadFile (self):

        Label(self,text='Nom du fichier : ').pack()
        self.nomFichier = Entry(self)
        self.nomFichier.pack()
        Label(self,text='').pack()
        self.buttonDownloadFile = Button(self, text='Download', command=self.downloadFile).pack()
        Label(self,text='').pack()
        self.buttonCancel = Button(self, text='Annuler', command=self.annulation).pack()


############################################################################




############################################################################




class uploadApp(Toplevel):

    def __init__(self,master):
        Toplevel.__init__(self,master)
        self.title("Partage fichier - Haddock Share")
        self.geometry("200x200")
        self.iconbitmap('haddockIco.ico')
        self.grab_set()
        self.packUploadFile()


    def annulation (self):

        self.destroy()


    def uploadFile (self):

        os.system('net use s: \\\\'+self.namePc.get()+'\\Users\\Public\\Haddock')
        filename = filedialog.askopenfilename(title="Ouvrir votre document",filetypes=[('all files','.*')])
        newName = self.nameFic.get()

        encrypt_file(filename,newName)
        os.system('net use /del S:')
        self.destroy()


    def packUploadFile (self):

        self.nomOrigFic = Label(self,text='Nom fichier à upload : ')
        self.nomOrigFic.pack()
        self.nameFic = Entry(self)
        self.nameFic.pack()
        self.nomPc = Label(self,text='Nom du Pc : ').pack()
        self.namePc = Entry(self)
        self.namePc.pack()
        Label(self,text='').pack()
        self.buttonDownloadFile = Button(self, text='Upload', command=self.uploadFile)
        self.buttonDownloadFile.pack()
        self.selectFile = Label(self,text=" ")
        self.selectFile.pack()
        self.buttonCancel = Button(self, text='Annuler', command=self.annulation).pack()



############################################################################




############################################################################

    



def upFile() :

    app = uploadApp(acceuil)

def downFile() :
        
    app = downloadApp(acceuil)

def createCo() :
    
    app = createApp(acceuil)
    buttonUF.pack(padx=5,pady=5)
    buttonDF.pack(padx=5,pady=5)
    buttonCreateAdHoc.pack_forget()
    buttonCoAdHoc.pack_forget()
    nomPc.pack(side=BOTTOM)
    buttonDeco.pack(side=BOTTOM)
    
def connectCo() :
    
    app = connectApp(acceuil)
    buttonDF.pack(padx=5,pady=5)
    buttonUF.pack(padx=5,pady=5)
    buttonCoAdHoc.pack_forget()
    buttonCreateAdHoc.pack_forget()
    nomPc.pack(side=BOTTOM)
    buttonDeco.pack(side=BOTTOM)
    

def deco() :

    os.system('netsh wlan disconnect')
    buttonCoAdHoc.pack(padx=5,pady=5)
    buttonCreateAdHoc.pack(padx=5,pady=5)
    buttonDF.pack_forget()
    buttonUF.pack_forget()
    buttonDeco.pack_forget()
    nomPc.pack_forget()
    os.system('rmdir /S/Q C:\\Users\\Public\\Haddock')
    os.system('net use /del S:')


def promptHelp() :

    aide = messagebox.showinfo('Aide',"    Si l'application ne fonctionne pas, verifiez les paramètres suivants :\n\n-Compatibilité de votre carte réseau : Pour verifier cette information, tapez 'netsh wlan show drivers' dans l'interface de commande. Si le paramètre réseau hebergé n'est pas à oui, vous ne pouvez pas créer de réseau.\n\n-Autorisation de partage : Des problèmes de connexion peuvent être rencontrés si vos paramètres de partage ne sont pas bien définies.\n Dans le centre de réseau et partage veuillez désactiver la protection d'échange de fichier par mot de passe, activer le partage des fichier et dossiers partagés.")



def on_closing():
    if messagebox.askokcancel("Quit", "Voulez vous vraiment quitter ?\nVous serez déconnecté du réseau."):
        acceuil.destroy()
        os.system('rmdir /S/Q C:\\Users\\Public\\Haddock')    
        os.system('netsh wlan disconnect')
        


############################################################################




############################################################################



###  DEFINITION ACCEUIL ###

acceuil = Tk()
acceuil.tk_strictMotif()
acceuil.geometry("500x500")
acceuil.title("Haddock Share")
acceuil.iconbitmap('haddockIco.ico')
acceuil.protocol("WM_DELETE_WINDOW", on_closing)
photo = PhotoImage(file="capitaineHaddock.gif")
canvas = Canvas(acceuil,width=356, height=356)
canvas.create_image(178,178,image=photo)
canvas.pack()



frame1 = Frame(acceuil)
frame2 = Frame(acceuil)

Label(frame1, text="    ").pack(side=LEFT)
Label(frame2, text="    ").pack(side=RIGHT)

buttonCoAdHoc = Button(frame1, text='Se Connecter à un Reseau', command=connectCo)
buttonCoAdHoc.pack(padx=5,pady=5)
buttonDF = Button(frame1, text="Download a File",command=downFile)

buttonCreateAdHoc = Button(frame1, text='Créer un Reseau', command=createCo)
buttonCreateAdHoc.pack(padx=5,pady=5)
buttonUF = Button(frame1, text="Upload a File",command=upFile)

Label(acceuil, text = ' ').pack(side=BOTTOM)
nomPc = Label(acceuil, text = os.popen('hostname').read())
buttonDeco = Button(acceuil, text="Se déconnecter",command=deco)


buttonHelp = Button(frame2, text="?",command=promptHelp).pack()

frame1.pack(side=LEFT,padx=20, pady=20)
frame2.pack(side=RIGHT,padx=20, pady=20)


###########################

acceuil.mainloop()



    
