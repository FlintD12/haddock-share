#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, random, struct, sys
from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
from Crypto.Cipher import AES
import rsa
import shutil
import zlib
import base64




def encrypt_file(key, in_filename, out_filename=None, chunksize=64*1024):

    ext = os.path.splitext(in_filename)[1]

    if not out_filename:
        out_filename = 'C:\\Users\\Public\\Haddock\\copy' + ext + '.enc'
    else :
        out_filename = 'C:\\Users\\Public\\Haddock\\' + out_filename + ext + '.enc'

    iv = ''.join(chr(random.randint(0, 0xFF)) for i in range(16))
    protego = AES.new(key, AES.MODE_CBC, iv)
    filesize = os.path.getsize(in_filename)

    with open(in_filename, 'rb') as infile:
        with open(out_filename, 'wb') as outfile:
            outfile.write(struct.pack('<Q', filesize))
            outfile.write(iv)

            while True:
                chunk = infile.read(chunksize)
                if len(chunk) == 0:
                    break
                elif len(chunk) % 16 != 0:
                    chunk += ' ' * (16-len(chunk) % 16)
            
                outfile.write(protego.encrypt(chunk))




def decrypt_file(key, in_filename, out_filename=None, chunksize=24*1024):


    if not out_filename:
        out_filename = filedialog.askdirectory()+"//adhoc"+in_filename[-8:-4]

    else :
        out_filename = filedialog.askdirectory()+ '\\' + out_filename+in_filename[-8:-4]
        
    with open(in_filename, 'rb') as infile:
        origsize = struct.unpack('<Q', infile.read(struct.calcsize('Q')))[0]
        iv = infile.read(16)
        decryptor = AES.new(key, AES.MODE_CBC, iv)

        with open(out_filename,'wb') as outfile:
            while True:
                chunk = infile.read(chunksize)
                if len(chunk) == 0:
                    break
                
                outfile.write(decryptor.decrypt(chunk))

            outfile.truncate(origsize)





############################################################################




############################################################################


class connectApp(Toplevel):

    def __init__(self,master):
        Toplevel.__init__(self,master)
        self.title("Connexion réseau - Haddock Share")
        self.geometry("200x200")
        self.iconbitmap('haddockIco.ico')
        self.grab_set()
        self.packCo()
        

    



#### si connexion ok, fermeture de la fenetre


    def connexion (self):

        reseauname = self.e1.get()
        mdp = self.passWord.get()

        contenuFic = '<?xml version="1.0"?><WLANProfile xmlns="http://www.microsoft.com/networking/WLAN/profile/v1"><name>'+reseauname+'</name><SSIDConfig><SSID><name>'+reseauname+'</name></SSID></SSIDConfig><connectionType>ESS</connectionType><connectionMode>auto</connectionMode><MSM><security><authEncryption><authentication>WPA2PSK</authentication><encryption>AES</encryption><useOneX>false</useOneX></authEncryption><sharedKey><keyType>passPhrase</keyType><protected>false</protected><keyMaterial>'+mdp+'</keyMaterial></sharedKey></security></MSM><MacRandomization xmlns="http://www.microsoft.com/networking/WLAN/profile/v3"><enableRandomization>false</enableRandomization></MacRandomization></WLANProfile>'
        ficXML = open("profile.xml", "w")
        ficXML.write(contenuFic)
        ficXML.close()
        os.system('netsh wlan add profile filename="profile.xml"')
        b = os.popen('netsh wlan connect name="'+reseauname+'"').read()
        os.system('mkdir C:\\Users\\Public\\Haddock')
        self.destroy()


    def annulation (self):

        deco()
        self.destroy()
        

    def packCo(self):

        
        self.nomReseauCo = Label(self,text='Nom du réseau : ').pack()
        self.e1 = Entry(self)
        self.e1.pack()

        self.mdpReseauCo = Label(self,text='Mdp réseau : ').pack()

        self.passWord = Entry(self)
        self.passWord.pack()
        Label(self,text='').pack()
        self.buttonConnect = Button(self, text='Connect', command=self.connexion).pack()
        Label(self,text='').pack()
        self.buttonCancel = Button(self, text='Annuler', command=self.annulation).pack()
        


############################################################################




############################################################################




class createApp(Toplevel):

    def __init__(self,master):
        Toplevel.__init__(self,master)
        self.title("Création réseau - Haddock Share")
        self.geometry("200x200")
        self.iconbitmap('haddockIco.ico')
        self.grab_set()
        self.packCreate()


    def annulation (self):

        deco()
        self.destroy()
        


    def creation (self):

        nomConnexion=self.e3.get()
        mdp=self.e4.get()
        os.system('netsh wlan set hostednetwork mode=allow ssid='+nomConnexion+' key='+mdp)
        os.system('netsh wlan start hostednetwork')
        os.system('mkdir C:\\Users\\Public\\Haddock')
        os.system('net share C:\\Users\\Public\\Haddock')
        self.destroy()

    def packCreate(self):

        self.nomReseauCreate = Label(self,text='Nom du réseau : ')
        self.nomReseauCreate.pack()
        self.e3 = Entry(self)
        self.e3.pack()
        self.mdpReseauCreate = Label(self,text='Mdp réseau : ').pack()
        self.e4 = Entry(self)
        self.e4.pack()
        Label(self,text='').pack()
        self.buttonCreate = Button(self, text='Create', command=self.creation).pack()
        Label(self,text='').pack()
        self.buttonCancel = Button(self, text='Annuler', command=self.annulation).pack()


############################################################################




############################################################################




class downloadApp(Toplevel):

    def __init__(self,master):
        Toplevel.__init__(self,master)
        self.title("Téléchargement fichier - Haddock Share")
        self.geometry("200x220")
        self.iconbitmap('haddockIco.ico')
        self.grab_set()
        self.packDownloadFile()

    def annulation (self):

        self.destroy()

    

    def downloadFile (self):

        os.system('net use s: \\\\'+self.namePc.get()+'\\Users\\Public\Haddock')
        filename = filedialog.askopenfilename(initialdir = "s:\\", title="Ouvrir votre document",filetypes=[('all files','.*')])
        key = self.keyD.get()
        newName = self.nomFichier.get()
        decrypt_file(key,filename,newName)
        os.system('net use /del S:')
        self.destroy()




    def packDownloadFile (self):

        self.keyDecrypt = Label(self,text='Clé déchiffrement : ')
        self.keyDecrypt.pack()
        self.keyD = Entry(self)
        self.keyD.pack()
        Label(self,text='Nom du fichier : ').pack()
        self.nomFichier = Entry(self)
        self.nomFichier.pack()
        self.nomPc = Label(self,text='Nom du Pc : ').pack()
        self.namePc = Entry(self)
        self.namePc.pack()
        Label(self,text='').pack()
        self.buttonDownloadFile = Button(self, text='Download File', command=self.downloadFile).pack()
        Label(self,text='').pack()
        self.buttonCancel = Button(self, text='Annuler', command=self.annulation).pack()


############################################################################




############################################################################




class uploadApp(Toplevel):

    def __init__(self,master):
        Toplevel.__init__(self,master)
        self.title("Partage fichier - Haddock Share")
        self.geometry("200x200")
        self.iconbitmap('haddockIco.ico')
        self.grab_set()
        self.packUploadFile()


    def annulation (self):

        self.destroy()


    def uploadFile (self):

        filename = filedialog.askopenfilename(title="Ouvrir votre document",filetypes=[('all files','.*')])
        newName = self.nameFic.get()

        key = self.keyC.get()
        encrypt_file(key,filename,newName)
        self.destroy()


    def packUploadFile (self):

        self.keyCrypt = Label(self,text='Clé chiffrement : ')
        self.keyCrypt.pack()
        self.keyC = Entry(self)
        self.keyC.pack()    
        self.nomOrigFic = Label(self,text='Nom fichier à upload : ')
        self.nomOrigFic.pack()
        self.nameFic = Entry(self)
        self.nameFic.pack()
        Label(self,text='').pack()
        self.buttonDownloadFile = Button(self, text='Upload File', command=self.uploadFile)
        self.buttonDownloadFile.pack()
        self.selectFile = Label(self,text=" ")
        self.selectFile.pack()
        self.buttonCancel = Button(self, text='Annuler', command=self.annulation).pack()



############################################################################




############################################################################

    



def upFile() :

    app = uploadApp(acceuil)

def downFile() :
        
    app = downloadApp(acceuil)

def createCo() :
    
    app = createApp(acceuil)
    buttonUF.pack()
    buttonDF.pack()
    buttonCreateAdHoc.pack_forget()
    buttonCoAdHoc.pack_forget()
    nomPc.pack(side=BOTTOM)
    buttonDeco.pack(side=BOTTOM)
    
def connectCo() :
    
    app = connectApp(acceuil)
    buttonDF.pack()
    buttonUF.pack()
    buttonCoAdHoc.pack_forget()
    buttonCreateAdHoc.pack_forget()
    nomPc.pack(side=BOTTOM)
    buttonDeco.pack(side=BOTTOM) 
    

def deco() :

    os.system('netsh wlan disconnect')
    buttonCoAdHoc.pack()
    buttonCreateAdHoc.pack()
    buttonDF.pack_forget()
    buttonUF.pack_forget()
    buttonDeco.pack_forget()
    nomPc.pack_forget()
    os.system('rmdir /S/Q C:\\Users\\Public\\Haddock')
    os.system('net use /del S:')

def on_closing():
    if messagebox.askokcancel("Quit", "Voulez vous vraiment quitter ?\nVous serez déconnecté du réseau."):
        acceuil.destroy()
        os.system('rmdir /S/Q C:\\Users\\Public\\Haddock')    
        os.system('netsh wlan disconnect')
        

###  DEFINITION ACCEUIL ###

acceuil = Tk()
acceuil.tk_strictMotif()
acceuil.geometry("500x500")
acceuil.title("Haddock Share")
acceuil.iconbitmap('haddockIco.ico')
acceuil.protocol("WM_DELETE_WINDOW", on_closing)
photo = PhotoImage(file="capitaineHaddock.gif")
canvas = Canvas(acceuil,width=356, height=356)
canvas.create_image(178,178,image=photo)
canvas.pack()



frame1 = Frame(acceuil)
frame2 = Frame(acceuil)

Label(frame1, text="    ").pack(side=LEFT)
Label(frame2, text="    ").pack(side=RIGHT)

buttonCoAdHoc = Button(frame1, text='Se Connecter à un Reseau', command=connectCo)
buttonCoAdHoc.pack()
Label(frame1, text=" ").pack()
buttonDF = Button(frame1, text="Download a File",command=downFile)

buttonCreateAdHoc = Button(frame2, text='Créer un Reseau', command=createCo)
buttonCreateAdHoc.pack()
Label(frame2, text=" ").pack()
buttonUF = Button(frame2, text="Upload a File",command=upFile)

Label(acceuil, text = ' ').pack(side=BOTTOM)
nomPc = Label(acceuil, text = os.popen('hostname').read())
buttonDeco = Button(acceuil, text="Se déconnecter",command=deco)


frame1.pack(side=LEFT,padx=30, pady=30)
frame2.pack(side=RIGHT,padx=30, pady=30)


###########################


acceuil.mainloop()




    
