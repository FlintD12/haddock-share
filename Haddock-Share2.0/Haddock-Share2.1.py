#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, random, struct, sys
from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
from Crypto.PublicKey import RSA
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
from Crypto.Cipher import PKCS1_OAEP
from params import path_to_sharefolder
from config import default_upload_folder
from config import default_download_folder
import gzip  
import rsa
import shutil
import zlib
import base64
import subprocess
import multiprocessing
import threading
import time
import concurrent.futures


CONNECTED = False
file_contents = []

def exec_AEScrypt(in_filename,out_filename,key,enc_key):
    print("starting...")
    start = time.perf_counter()
    key = get_random_bytes(16)
    
    os.system(f"..\\aescrypt.exe -e -p {key} {in_filename}")
    print(in_filename)
    print(out_filename)
    end = time.perf_counter()
    print (f"Encryption time : {end - start:0.4f} secondes")
    start = time.perf_counter()
    # origin = r''+in_filename+'.aes'
    # dest = r''+out_filename+'.aes'
    # shutil.copyfile(origin,dest)
    end = time.perf_counter()
    print (f"Copy time : {end - start:0.4f} secondes")
    


def exec_write(blob,key,enc_key,out_filename):
    print("starting...")
    start = time.perf_counter()
    cipher_aes = AES.new(key, AES.MODE_EAX)
    cipher,tag = cipher_aes.encrypt_and_digest(blob)
    fd = open(out_filename+'.gz.enc', "wb")
       
    [ fd.write(x) for x in (enc_key, cipher_aes.nonce, tag,cipher)]
    fd.close()
    end = time.perf_counter()
    print (f"Encryption time : {end - start:0.4f} secondes")



class MonThread (threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
    
    def run(self):
        new_key = RSA.generate(4096, e=65537)
        #The private key in PEM format
        private_key = new_key.exportKey("PEM")

        #The public key in PEM Format
        public_key = new_key.publickey().exportKey("PEM")

        os.system(f'mkdir {path_to_sharefolder}')
        os.system(f'net share {path_to_sharefolder}')

        fd = open("private_key.pem", "wb")
        fd.write(private_key)
        fd.close()

        fd = open(f"{path_to_sharefolder}\\public_key.pem", "wb")
        fd.write(public_key)
        fd.close()
        buttonUF.configure(state=NORMAL)
        buttonDF.configure(state=NORMAL)
        show_files()
        print("keys generated")

# class CryptThread (threading.Thread):
#     def __init__(self,blob,key,enc_key,out_filename):
#         threading.Thread.__init__(self)
#         self.blob = blob
#         self.key = key
#         self.enc_key = enc_key
#         self.out_filename = out_filename

#     def run(self):
#         exec_write(self.blob,self.key,self.enc_key,self.out_filename)
        
############################################################################




############################################################################

# https://medium.com/@ismailakkila/black-hat-python-encrypt-and-decrypt-with-rsa-cryptography-bd6df84d65bc

#Our Encryption Function
def encrypt_lilblob(blob, public_key,out_filename):

    #Import the Public Key and use for encryption using PKCS1_OAEP
    rsa_key = RSA.importKey(public_key)
    rsa_key = PKCS1_OAEP.new(rsa_key)

    key = get_random_bytes(16)
    enc_key = rsa_key.encrypt(key)
    
    t = threading.Thread(target=exec_write,args=[blob,key,enc_key,out_filename])
    t.start()
    
    
#Our Encryption Function
def encrypt_bigblob(in_filename,out_filename,public_key):

    #Import the Public Key and use for encryption using PKCS1_OAEP
    rsa_key = RSA.importKey(public_key)
    rsa_key = PKCS1_OAEP.new(rsa_key)

    key = get_random_bytes(16)
    enc_key = rsa_key.encrypt(key)
    
    t = threading.Thread(target=exec_AEScrypt,args=[in_filename,out_filename,key,enc_key])
    t.start()


#Our Decryption Function
def decrypt_blob(private_key, in_filename, out_filename):

    #Import the Private Key and use for decryption using PKCS1_OAEP
    rsakey = RSA.importKey(private_key)
    path = os.path.splitext(in_filename)[0]
    ext = path.split('.')[len(path.split('.'))-2]
    print(ext)
    fd = open(in_filename, "rb")
    enc_key, nonce, tag, ciphertext = [ fd.read(x) for x in (rsakey.size_in_bytes(), 16, 16, -1)]
    fd.close()

    cipher_rsa = PKCS1_OAEP.new(rsakey)
    key = cipher_rsa.decrypt(enc_key)

    cipher_aes = AES.new(key, AES.MODE_EAX, nonce)
    data = cipher_aes.decrypt_and_verify(ciphertext, tag)

    fd = open(out_filename+'.gz', "wb")
    fd.write(data)
    fd.close()
    
    fp = open(out_filename,"wb")
        
    if ext not in ["zip","mp4"] : 
        with gzip.open(out_filename+".gz","rb") as f:
            bindata = f.read()
            fp.write(bindata)
            f.close()
    else :
        fs = open(out_filename+'.gz',"rb")
        data = fs.read()
        fp.write(data)
        fs.close()
    os.remove(out_filename+".gz")
    fp.close()    
	

def encrypt_file(in_filename, out_filename=None, chunksize=64*1024):

    ext = os.path.splitext(in_filename)[1]
    in_name = in_filename.split('/')
    print(ext)
    if not out_filename:
        out_filename = 's:\\' + in_name[len(in_name)-1] 
    else :
        out_filename = 's:\\' + out_filename + ext 

    fd = open("s:\\public_key.pem", "rb")
    public_key = fd.read()
    fd.close()

    if ext not in [".zip",".mp4"] : 
        start = time.perf_counter()
        with open(in_filename, 'rb') as f_in:
            with gzip.open('tmp.gz', 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)

        fd = open('tmp.gz', "rb")
        unencrypted_blob = fd.read()
        fd.close()
        os.remove("tmp.gz")
        end = time.perf_counter()
        print(f"Compression time : {end-start:0.4f} secondes")
        encrypt_lilblob(unencrypted_blob, public_key,out_filename)

    else :
        encrypt_bigblob(in_filename, out_filename, public_key)
    
    




def decrypt_file(in_filename, out_filename=None, chunksize=24*1024):


    if not out_filename:
        out_filename = filedialog.askdirectory(initialdir=default_download_folder)+"//adhoc."+in_filename.split(".")[1]

    else :
        out_filename = filedialog.askdirectory(initialdir=default_download_folder)+ '\\' + out_filename+"."+in_filename.split(".")[1]
        
    fd = open("private_key.pem", "rb")
    private_key = fd.read()
    fd.close()

    decrypt_blob(private_key, in_filename, out_filename)



def connectNetwork(reseauname) :

    b = os.popen('netsh wlan connect name="'+reseauname+'"').read()
    m = MonThread()
    m.start()
    
    v.set("Status : Connected to : "+reseauname)
    global CONNECTED 
    CONNECTED= True
    buttonDF.pack(padx=5,pady=5)
    buttonUF.pack(padx=5,pady=5)
    buttonUF.configure(state=DISABLED)
    buttonDF.configure(state=DISABLED)
    buttonCoAdHoc.pack_forget()
    buttonCreateAdHoc.pack_forget()
    buttonDeco.pack(side=BOTTOM)


############################################################################




############################################################################


class connectApp(Toplevel):

    def __init__(self,master):
        Toplevel.__init__(self,master)
        self.title("Connexion réseau - Haddock Share")
        self.geometry("200x200")
        self.iconbitmap('haddockIco.ico')
        self.grab_set()
        self.packCo()
        

    

    def connexion (self):

        reseauname = self.e1.get()
        mdp = self.passWord.get()

        contenuFic = '<?xml version="1.0"?><WLANProfile xmlns="http://www.microsoft.com/networking/WLAN/profile/v1"><name>'+reseauname+'</name><SSIDConfig><SSID><name>'+reseauname+'</name></SSID></SSIDConfig><connectionType>ESS</connectionType><connectionMode>auto</connectionMode><MSM><security><authEncryption><authentication>WPA2PSK</authentication><encryption>AES</encryption><useOneX>false</useOneX></authEncryption><sharedKey><keyType>passPhrase</keyType><protected>false</protected><keyMaterial>'+mdp+'</keyMaterial></sharedKey></security></MSM><MacRandomization xmlns="http://www.microsoft.com/networking/WLAN/profile/v3"><enableRandomization>false</enableRandomization></MacRandomization></WLANProfile>'
        ficXML = open("profile.xml", "w")
        ficXML.write(contenuFic)
        ficXML.close()
        os.system('netsh wlan add profile filename="profile.xml"')
        connectNetwork(reseauname)
        self.destroy()

    def annulation (self):

        deco()
        self.destroy()
        

    def packCo(self):

        v = StringVar()
        w = StringVar()
        
        self.nomReseauCo = Label(self,text='Nom du réseau : ').pack()
        self.e1 = Entry(self, textvariable=v)
        self.e1.pack()
        v.set("toto")

        self.mdpReseauCo = Label(self,text='Mdp réseau : ').pack()

        self.passWord = Entry(self, show="*", textvariable=w)
        self.passWord.pack()
        w.set("1234567890")
        Label(self,text='').pack()
        self.buttonConnect = Button(self, text='Connecter', command=self.connexion).pack()
        Label(self,text='').pack()
        self.buttonCancel = Button(self, text='Annuler', command=self.annulation).pack()
        
    

############################################################################




############################################################################




class createApp(Toplevel):

    def __init__(self,master):
        Toplevel.__init__(self,master)
        self.title("Création réseau - Haddock Share")
        self.geometry("200x200")
        self.iconbitmap('haddockIco.ico')
        self.grab_set()
        self.packCreate()


    def annulation (self):

        deco()
        self.destroy()
        


    def creation (self):
        
        nomConnexion=self.e3.get()
        mdp=self.e4.get()
        os.system('netsh wlan set hostednetwork mode=allow ssid='+nomConnexion+' key='+mdp)
        os.system('netsh wlan start hostednetwork')
        m = MonThread()
        m.start()
        buttonDeco.pack(side=BOTTOM)
        global CONNECTED
        CONNECTED = True
        
        self.destroy()

    def packCreate(self):

        v = StringVar()
        w = StringVar()

        self.nomReseauCreate = Label(self,text='Nom du réseau : ')
        self.nomReseauCreate.pack()
        self.e3 = Entry(self, textvariable=v)
        self.e3.pack()
        v.set("toto")
        self.mdpReseauCreate = Label(self,text='Mdp réseau : ').pack()
        self.e4 = Entry(self,textvariable=w)
        self.e4.pack()
        w.set("1234567890")
        Label(self,text='').pack()
        self.buttonCreate = Button(self, text='Create', command=self.creation).pack()
        Label(self,text='').pack()
        self.buttonCancel = Button(self, text='Annuler', command=self.annulation).pack()


############################################################################




############################################################################




class downloadApp(Toplevel):

    def __init__(self,master):
        Toplevel.__init__(self,master)
        self.title("Téléchargement fichier - Haddock Share")
        self.geometry("200x220")
        self.iconbitmap('haddockIco.ico')
        self.grab_set()
        self.packDownloadFile()

    def annulation (self):

        self.destroy()

    

    def downloadFile (self):

        
        filename = filedialog.askopenfilename(initialdir = f"{path_to_sharefolder}", title="Ouvrir votre document",filetypes=[('all files','.*')])
        newName = self.nomFichier.get()
        decrypt_file(filename,newName)
        self.destroy()




    def packDownloadFile (self):

        Label(self,text='Nom du fichier : ').pack()
        self.nomFichier = Entry(self)
        self.nomFichier.pack()
        Label(self,text='').pack()
        self.buttonDownloadFile = Button(self, text='Download', command=self.downloadFile).pack()
        Label(self,text='').pack()
        self.buttonCancel = Button(self, text='Annuler', command=self.annulation).pack()


############################################################################




############################################################################




class uploadApp(Toplevel):

    def __init__(self,master):
        Toplevel.__init__(self,master)
        self.title("Partage fichier - Haddock Share")
        self.geometry("200x200")
        self.iconbitmap('haddockIco.ico')
        self.grab_set()
        self.packUploadFile()


    def annulation (self):

        self.destroy()


    def uploadFile (self):

        os.system('net use s: \\\\'+self.namePc.get()+'\\Users\\Public\\Haddock')
        filename = filedialog.askopenfilename(initialdir=default_upload_folder,title="Ouvrir votre document",filetypes=[('all files','.*')])
        newName = self.nameFic.get()

        encrypt_file(filename,newName)
        self.destroy()


    def packUploadFile (self):

        v = StringVar()

        self.nomOrigFic = Label(self,text='Nom fichier à upload : ')
        self.nomOrigFic.pack()
        self.nameFic = Entry(self)
        self.nameFic.pack()
        self.nomPc = Label(self,text='Nom du Pc : ').pack()
        self.namePc = Entry(self, textvariable=v)
        v.set("DESKTOP-7VDAB86")
        self.namePc.pack()
        Label(self,text='').pack()
        self.buttonDownloadFile = Button(self, text='Upload', command=self.uploadFile)
        self.buttonDownloadFile.pack()
        self.selectFile = Label(self,text=" ")
        self.selectFile.pack()
        self.buttonCancel = Button(self, text='Annuler', command=self.annulation).pack()


############################################################################




############################################################################

    



def upFile() :

    app = uploadApp(acceuil)

def downFile() :
        
    app = downloadApp(acceuil)

def createCo() :

    os.system('netsh wlan disconnect')
    app = createApp(acceuil)
    buttonUF.pack(padx=5,pady=5)
    buttonDF.pack(padx=5,pady=5)
    buttonUF.configure(state=DISABLED)
    buttonDF.configure(state=DISABLED)
    buttonCreateAdHoc.pack_forget()
    buttonCoAdHoc.pack_forget()
    nomPc.pack(side=BOTTOM)
    buttonDeco.pack(side=BOTTOM)
    show_files()
    
def connectCo() :
    
    os.system('netsh wlan disconnect')
    app = connectApp(acceuil)
    

def onselect(evt):

    w = evt.widget
    index = int(w.curselection()[0])
    value = w.get(index)
    connectNetwork(value)

def show_files():

    distList = os.listdir(path_to_sharefolder)
    listSel.delete(0,END)
    for item in distList:
        listSel.insert(END,item)

def show_networks():
    
    resp = str(subprocess.check_output("netsh wlan show networks", shell=True))
    split_resp = resp.split("SSID")[1:]    
    listSel.delete(0,END)
    for i in range(len(split_resp)):
        tmp = split_resp[i][8:]
        listSel.insert(END, tmp.split("\\")[0])

def show_refresh():

    if CONNECTED :
        show_files()
    else :
        show_networks()

def deco() :

    os.system('netsh wlan disconnect')
    buttonCoAdHoc.pack(padx=5,pady=5)
    buttonCreateAdHoc.pack(padx=5,pady=5)
    buttonDF.pack_forget()
    buttonUF.pack_forget()
    buttonDeco.pack_forget()
    os.system(f'rmdir /S/Q {path_to_sharefolder}')
    os.system('net use /del S:')
    global CONNECTED 
    CONNECTED = False
    v.set("Status : Not Connected")

    show_networks()


def promptHelp() :

    aide = messagebox.showinfo('Aide',"    Si l'application ne fonctionne pas, verifiez les paramètres suivants :\n\n-Compatibilité de votre carte réseau : Pour verifier cette information, tapez 'netsh wlan show drivers' dans l'interface de commande. Si le paramètre réseau hebergé n'est pas à oui, vous ne pouvez pas créer de réseau.\n\n-Autorisation de partage : Des problèmes de connexion peuvent être rencontrés si vos paramètres de partage ne sont pas bien définies.\n Dans le centre de réseau et partage veuillez désactiver la protection d'échange de fichier par mot de passe, activer le partage des fichier et dossiers partagés.")



def on_closing():
    global CONNECTED
    if CONNECTED :
        if messagebox.askokcancel("Quit", "Voulez vous vraiment quitter ?\nVous serez déconnecté du réseau."):
            acceuil.destroy()
            os.system(f'rmdir /S/Q {path_to_sharefolder}')    
            os.system('netsh wlan disconnect')
    else:
        acceuil.destroy()

def thread_reload():

    while True:
        time.sleep(5)
        print("refresh")
        try :
            show_refresh()
        except :
            pass

############################################################################




############################################################################

th = threading.Thread(target=thread_reload)
th.daemon = True
th.start()

###  DEFINITION ACCEUIL ###

acceuil = Tk()
acceuil.tk_strictMotif()
acceuil.geometry("500x650")
acceuil.title("Haddock Share")
acceuil.iconbitmap('haddockIco.ico')
acceuil.protocol("WM_DELETE_WINDOW", on_closing)
acceuil.configure()

photo = PhotoImage(file="capitaineHaddock.png")
canvas = Canvas(acceuil,width=300, height=300, highlightthickness=0)
canvas.create_image(178,178,image=photo)
canvas.pack()



frame1 = Frame(acceuil)
frame2 = Frame(acceuil)


buttonCoAdHoc = Button(frame1, text='Se Connecter à un Reseau', command=connectCo)
buttonCoAdHoc.pack(padx=5,pady=5)
buttonDF = Button(frame1, text="Download a File",command=downFile)

resp = str(subprocess.check_output("netsh wlan show drivers", shell=True))
rh_accepted = resp.split("\\xff")[5][2:5]
buttonCreateAdHoc = Button(frame1, text='Créer un Reseau', command=createCo)
buttonCreateAdHoc.pack(padx=5,pady=5)
if rh_accepted == "non" :
    buttonCreateAdHoc.configure(state=DISABLED)
buttonUF = Button(frame1, text="Upload a File",command=upFile)

Label(acceuil, text = ' ').pack(side=BOTTOM)
buttonDeco = Button(frame1, text="Se déconnecter",command=deco)

listSel = Listbox(frame2)
listSel.bind('<Double-1>', onselect)
listSel.pack()

Label(frame2, text=" ").pack()

buttonRefreshNetwork = Button(frame2, text="Refresh", command=show_refresh)
buttonRefreshNetwork.pack(side=BOTTOM)
show_networks()

v = StringVar()
buttonHelp = Button(acceuil, text="?",command=promptHelp).pack(side=BOTTOM)
stateLabel = Label(acceuil, textvariable=v).pack(side=BOTTOM)
nomPc = Label(acceuil, text = 'Machine name : '+os.popen('hostname').read()).pack(side=BOTTOM)
v.set("Network-Status : Not Connected")
frame1.pack(side=LEFT,padx=20, pady=20)
frame2.pack(side=RIGHT,padx=50)

###########################

acceuil.mainloop()


    
